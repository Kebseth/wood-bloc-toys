package com.WoodBlockToys.demo.model;

public class User {
    private String id;
    private String name;
    private String firstName;
    private String mail;
    private String adress;
    private String phoneNumber;

    public User(String id, String name, String firstName, String mail, String adress, String phoneNumber) {
        this.id          = id;
        this.name        = name;
        this.firstName   = firstName;
        this.mail        = mail;
        this.adress      = adress;
        this.phoneNumber = phoneNumber;
    }

    public String getId() { return id; }
    public String getPhoneNumber() { return phoneNumber; }
    public String getName() { return name; }
    public String getFirstName() { return firstName; }
    public String getMail() { return mail; }
    public String getAdress() { return adress; }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public void setAdress(String adress) {
        this.adress = adress;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }
}

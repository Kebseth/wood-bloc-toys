drop table if exists bloc;
drop table if exists "user";

create table bloc(
    id text primary key ,
    height int not null,
    base text not null ,
    color text not null ,
    finition text not null ,
    essence text not null
);

create table "user"(
    id text primary key ,
    name text not null ,
    firstName text not null ,
    mail text not null ,
    adresse text not null,
    password text not null ,
    phoneNumber text not null
)
